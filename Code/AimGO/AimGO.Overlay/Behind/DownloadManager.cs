﻿using Android.Content;
using Plugin.DownloadManager;
using Plugin.DownloadManager.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;

namespace AimGO.Overlay.Behind
{
    public static class DownloadManager
    {

        public static event Action<bool> DownloadEnded;
        private static IDownloadManager Downloader { get; }
        private static List<(int path, string name)> Scripts { get; }


        static DownloadManager()
        {
            Downloader = CrossDownloadManager.Current;
            (Downloader as DownloadManagerImplementation).IsVisibleInDownloadsUi = true;
            Scripts = new List<(int path, string name)>
            {
                ( Resource.String.throw_sh_path,"throw.sh"),
                ( Resource.String.scrennshot_sh_path,"screenshot.sh"),
                ( Resource.String.recognize_sh_path,"recognize.sh"),
                ( Resource.String.scheduler_sh_path,"scheduler.sh"),
                ( Resource.String.appraise_sh_path,"appraise.sh"),
                ( Resource.String.berry_sh_path,"berry.sh"),
                ( Resource.String.open_gift_sh_path,"open_gift.sh"),
                ( Resource.String.send_gift_sh_path,"send_gift.sh"),
                ( Resource.String.pokemon_database_path,"data_EN.csv"),
            };

        }

        public static bool AreScriptsInstalled(Context context)
        {
            return AreScriptsInstalled(Utils.Download(context));
        }

        public static bool InstallScripts(Context context)
        {
            foreach (var (path, name) in Scripts)
                DownloadFile(context, path, name);

            if (!IsTesseractLocalCopyInstalled(context))
                DownloadFile(context, Resource.String.tesseract_bin_path, "tesseract");

            if (!IsTesseractDataInstalled(context))
                DownloadFile(context, Resource.String.tesseract_data_eng_path, "eng.traineddata");


            return true;
        }

        public static bool InstallTesseract(Context context)
        {
            if (!IsTesseractInstalled())
            {
                try
                {
                    Java.Lang.Runtime.GetRuntime().Exec($"su -c cp {Utils.Download(context)}/tesseract /system/bin").WaitFor();
                    Java.Lang.Runtime.GetRuntime().Exec($"su -c chown root.root /system/bin/tesseract").WaitFor();
                    Java.Lang.Runtime.GetRuntime().Exec($"su -c chmod 755 /system/bin/tesseract").WaitFor();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }


        public static bool IsTesseractInstalled()
        {
            try
            {
                Java.Lang.Runtime.GetRuntime().Exec($"tesseract");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static bool IsTesseractLocalCopyInstalled(Context context)
        {
            return File.Exists($"{Utils.Download(context)}/tesseract");
        }

        public static bool IsTesseractDataInstalled(Context context)
        {
            return File.Exists($"{Utils.Download(context)}/eng.traineddata");
        }

        public static bool UninstallEverything(Context context)
        {
            try
            {

                foreach (var (_, name) in Scripts)
                    Java.Lang.Runtime.GetRuntime().Exec($"rm {Utils.Download(context)}/{name}");

                Java.Lang.Runtime.GetRuntime().Exec($"rm {Utils.Download(context)}/eng.traineddata");
                Java.Lang.Runtime.GetRuntime().Exec($"su -c rm /system/bin/tesseract");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        private static bool AreScriptsInstalled(string path)
        {
            int scriptsInstalled = 0;
            foreach (var (_, name) in Scripts)
                if (File.Exists($"{path}/{name}"))
                    scriptsInstalled++;

            return scriptsInstalled == Scripts.Count;
        }


        private static void DownloadFile(Context context, int url, string filename)
        {
            Downloader.PathNameForDownloadedFile = new Func<IDownloadFile, string>(file =>
            {
                return Path.Combine(context.GetExternalFilesDir(Android.OS.Environment.DirectoryDownloads).AbsolutePath, filename);
            });


            var url_string = context.Resources.GetString(url);
            var file = Downloader.CreateDownloadFile(url_string);
            Downloader.Start(file);
            file.PropertyChanged += File_PropertyChanged;
        }

        private static void File_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IDownloadFile.Status))
            {
                if (((IDownloadFile)sender).Status == DownloadFileStatus.COMPLETED)
                {
                    DownloadEnded?.Invoke(true);
                }
                else if (((IDownloadFile)sender).Status == DownloadFileStatus.FAILED)
                {
                    DownloadEnded?.Invoke(false);
                }
            }
        }

    }
}