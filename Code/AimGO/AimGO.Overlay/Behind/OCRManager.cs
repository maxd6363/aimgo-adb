﻿using Android.Content;
using System.IO;
using System.Text;

namespace AimGO.Overlay.Behind
{
    public class OCRManager
    {
        public PokemonManager PokemonManager { get; }

        private static Context LastContext { get; set; }


        public OCRManager(Context context)
        {
            LastContext = context;
            PokemonManager = new PokemonManager(LastContext);
        }



        public string Recognize(Context context)
        {
            LastContext = context;
            return Recognize(Utils.Download(context));
        }


        public string Recognize(string path)
        {

            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {path}/scheduler.sh {path}/").WaitFor();

            if (File.Exists($"{path}/outputCleaned.txt"))
            {
                var result = File.ReadAllText($"{path}/outputCleaned.txt");
                return result;
            }
            return "Error";
        }

        public string Clean(string input)
        {
            StringBuilder stringBuilder = new StringBuilder();
            input = Utils.Clean(input);
            var splited = input.Split(' ');
            int count = 0;
            foreach (var s in splited)
            {
                if (s.Length >= 3)
                {
                    stringBuilder.Append(s);
                    stringBuilder.Append(" ");
                    count++;
                }
            }
            return $"{count} : {stringBuilder}";
        }


        public Pokemon Find(string input)
        {
            if (!PokemonManager.CorrectlyLoaded)
            {
                return new Pokemon("Manager not initialized", -1);
            }

            var splited = input.Split(' ');
            Pokemon found = null;
            foreach (var s in splited)
            {
                found = PokemonManager.Find(s);
                if (found != null)
                    return found;
            }
            return found;
        }

    }
}