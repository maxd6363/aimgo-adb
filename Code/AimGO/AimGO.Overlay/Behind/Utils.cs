﻿using Android.Content;
using System.Text.RegularExpressions;

namespace AimGO.Overlay.Behind
{
    public static class Utils
    {

        public static string Download(Context context)
        {
            return context.GetExternalFilesDir(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
        }

        public static string Clean(string str)
        {
            str = Regex.Replace(str, @"\s+", " ");
            return Regex.Replace(str, "[^a-zA-Z_. ]+", "", RegexOptions.Compiled);
        }


    }
}