﻿namespace AimGO.Overlay.Behind
{
    public enum Berry
    {
        Razz,
        Nanab,
        Pinap,
        GoldenRazz,
        SilverPinap
    }
}