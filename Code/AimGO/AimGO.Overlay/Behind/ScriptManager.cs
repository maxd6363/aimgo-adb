﻿using AimGO.Overlay.Behind;
using Android.Content;
using System;

namespace AimGO.Overlay
{
    public static class ScriptManager
    {

        public static bool TakeScreenshot(Context context)
        {
            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/screenshot.sh {Utils.Download(context)}/screen.png");
            return true;
        }







        public static bool Throw(Context context, int percent)
        {
            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/throw.sh {percent}");
            return true;
        }

        public static bool Throw(Context context, Berry berry)
        {
            int indexBerry = -1;
            switch (berry)
            {
                case Berry.Razz:
                    indexBerry = 1;
                    break;
                case Berry.Nanab:
                    indexBerry = 2;
                    break;
                case Berry.Pinap:
                    indexBerry = 3;
                    break;
                case Berry.GoldenRazz:
                    indexBerry = -1;
                    break;
                case Berry.SilverPinap:
                    indexBerry = -1;
                    break;
            }
            if (indexBerry != -1)
            {
                Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/berry.sh {indexBerry}");
                return true;
            }

            return false;
        }


        public static bool OpenGift(Context context)
        {
            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/open_gift.sh");
            return true;
        }

        public static bool SendGift(Context context)
        {
            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/send_gift.sh");
            return true;
        }


        public static bool Appraise(Context context)
        {
            Java.Lang.Runtime.GetRuntime().Exec($"su -c sh {Utils.Download(context)}/appraise.sh");
            return true;
        }


        public static bool IsRoot()
        {
            try
            {
                Java.Lang.Runtime.GetRuntime().Exec($"su");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }










    }
}