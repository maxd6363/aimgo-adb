﻿namespace AimGO.Overlay.Behind
{
    public class Pokemon
    {
        public string Name { get; }
        public double Distance { get; }

        public Pokemon(string name, double distance)
        {
            Name = name;
            Distance = distance;
        }
        public override string ToString()
        {
            return $"{Name} : {Distance}";
        }
    }
}