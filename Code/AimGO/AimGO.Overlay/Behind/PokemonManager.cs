﻿using Android.Content;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace AimGO.Overlay.Behind
{
    public class PokemonManager
    {
        public List<Pokemon> Pokemons { get; }
        public bool CorrectlyLoaded { get; private set; }

        private Context Context { get; }



        public PokemonManager(Context context)
        {
            Pokemons = new List<Pokemon>();
            Context = context;

            CorrectlyLoaded = LoadPokemons();

        }


        public Pokemon Find(string name)
        {
            var index = Pokemons.FindIndex(x => x.Name == name);
            return index != -1 ? Pokemons[index] : null;
        }

        private bool LoadPokemons()
        {
            try
            {

                var path = Path.Combine(Utils.Download(Context), "data_EN.csv");
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    string[] columns = line.Split(',');
                    Pokemons.Add(new Pokemon(columns[0], double.Parse(columns[1], CultureInfo.InvariantCulture)));
                }
            }
            catch (Exception e)
            {
                Toast.MakeText(Context, e.Message, ToastLength.Long).Show();
                return false;
            }

            return true;

        }
    }
}