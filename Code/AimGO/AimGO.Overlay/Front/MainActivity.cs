﻿using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Net;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Widget;
using AndroidX.AppCompat.App;

namespace AimGO.Overlay.Front
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {

        private Button buttonStart;
        private Button buttonStartAI;
        private Button buttonStop;
        private Button buttonIntall;
        private Button buttonUninstall;
        private Button buttonRoot;
        private Button buttonTesseract;
        private Intent floatingIntent;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            buttonStartAI = FindViewById<Button>(Resource.Id.buttonStartAI);
            buttonStart = FindViewById<Button>(Resource.Id.buttonStart);
            buttonStop = FindViewById<Button>(Resource.Id.buttonStop);
            buttonIntall = FindViewById<Button>(Resource.Id.buttonInstall);
            buttonUninstall = FindViewById<Button>(Resource.Id.buttonUninstall);
            buttonRoot = FindViewById<Button>(Resource.Id.buttonRoot);
            buttonTesseract = FindViewById<Button>(Resource.Id.buttonTesseract);

            buttonStartAI.Click += ButtonStartAI_Click;
            buttonStart.Click += ButtonStart_Click;
            buttonStop.Click += ButtonStop_Click;
            buttonIntall.Click += ButtonIntall_Click;
            buttonUninstall.Click += ButtonUninstall_Click; ;
            buttonRoot.Click += ButtonRoot_Click;
            buttonTesseract.Click += ButtonTesseract_Click;

            Behind.DownloadManager.DownloadEnded += ScriptManager_DowloadEned;

            UpdateScriptButton();
            UpdateTesseractButton();


            if (Behind.DownloadManager.AreScriptsInstalled(this))
            {
                UpdateRootButton();
            }

        }

        private void ButtonUninstall_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this, (Behind.DownloadManager.UninstallEverything(this) ? "Uninstalled !" : "Failed to unistall"), ToastLength.Long).Show();


            UpdateScriptButton();
            UpdateTesseractButton();
        }

        private void ButtonTesseract_Click(object sender, System.EventArgs e)
        {
            Behind.DownloadManager.InstallTesseract(this);
            UpdateTesseractButton();
        }

        private void ButtonStartAI_Click(object sender, System.EventArgs e)
        {
            if (!Settings.CanDrawOverlays(this))
            {
                StartActivityForResult(new Intent(Settings.ActionManageOverlayPermission, Uri.Parse("package:" + PackageName)), 1);
            }
            else
            {
                floatingIntent = new Intent(this, typeof(AIFloatingService));
                StartService(floatingIntent);
            }
        }

        private void UpdateScriptButton()
        {
            buttonIntall.BackgroundTintList = ColorStateList.ValueOf(Behind.DownloadManager.AreScriptsInstalled(this) ? Android.Graphics.Color.Green : Android.Graphics.Color.Red);
        }


        private void UpdateRootButton()
        {
            buttonRoot.BackgroundTintList = ColorStateList.ValueOf(ScriptManager.IsRoot() ? Android.Graphics.Color.Green : Android.Graphics.Color.Red);
        }

        private void UpdateTesseractButton()
        {
            buttonTesseract.BackgroundTintList = ColorStateList.ValueOf(Behind.DownloadManager.IsTesseractInstalled() ? Android.Graphics.Color.Green : Android.Graphics.Color.Red);
        }

        private void ScriptManager_DowloadEned(bool obj)
        {
            if (Behind.DownloadManager.AreScriptsInstalled(this))
            {
                Toast.MakeText(this, obj ? "Download finished" : "Download failed", ToastLength.Short).Show();
                UpdateScriptButton();
                UpdateRootButton();
            }
            if (Behind.DownloadManager.IsTesseractInstalled())
            {
                UpdateTesseractButton();
            }

        }

        private void ButtonRoot_Click(object sender, System.EventArgs e)
        {
            UpdateRootButton();
        }

        private void ButtonIntall_Click(object sender, System.EventArgs e)
        {
            Behind.DownloadManager.InstallScripts(this);

            Toast.MakeText(this, "Downloading scripts", ToastLength.Short).Show();
        }

        private void ButtonStart_Click(object sender, System.EventArgs e)
        {
            if (!Settings.CanDrawOverlays(this))
            {
                StartActivityForResult(new Intent(Settings.ActionManageOverlayPermission, Uri.Parse("package:" + PackageName)), 1);
            }
            else
            {
                floatingIntent = new Intent(this, typeof(PrettyFloatingService));
                StartService(floatingIntent);
            }
        }


        private void ButtonStop_Click(object sender, System.EventArgs e)
        {
            Application.Context.StopService(new Intent(this, typeof(PrettyFloatingService)));
            Application.Context.StopService(new Intent(this, typeof(AIFloatingService)));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }



    }
}