﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;

namespace AimGO.Overlay.Front
{
    [Service(Name = "com.companyname.aimgo.overlay.PrettyFloatingService")]
    class PrettyFloatingService : Service
    {
        private WindowManagerLayoutParams layoutParams;
        private IWindowManager windowManager;
        private View floatView;
        private int x;
        private int y;
        private SeekBar seekBar;
        private TextView force;

        public override void OnCreate()
        {
            ShowFloatingWindow();
            base.OnCreate();
        }


        public override IBinder OnBind(Intent intent)
        {
            return null;
        }


        private void ShowFloatingWindow()
        {
            windowManager = GetSystemService(WindowService).JavaCast<IWindowManager>();
            LayoutInflater mLayoutInflater = LayoutInflater.From(ApplicationContext);

            floatView = mLayoutInflater.Inflate(Resource.Layout.pretty_floating_layout, null);

            floatView.Touch += FloatView_Touch;

            ImageView buttonThrow = floatView.FindViewById<ImageView>(Resource.Id.buttonThrow);
            seekBar = floatView.FindViewById<SeekBar>(Resource.Id.seekBar);
            force = floatView.FindViewById<TextView>(Resource.Id.textViewForce);

            seekBar.ProgressChanged += SeekBar_ProgressChanged;
            buttonThrow.Click += ButtonThrow_Click;



            layoutParams = new WindowManagerLayoutParams
            {
                Type = WindowManagerTypes.ApplicationOverlay,
                Flags = WindowManagerFlags.NotFocusable,
                Width = Resources.DisplayMetrics.WidthPixels / 2,
                Height = Resources.DisplayMetrics.HeightPixels / 15,
                Format = Android.Graphics.Format.Translucent,
            };
            windowManager.AddView(floatView, layoutParams);
        }

        private void SeekBar_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            force.Text = e.Progress.ToString();
        }

        private void FloatView_Touch(object sender, View.TouchEventArgs e)
        {
            switch (e.Event.Action)
            {

                case MotionEventActions.Down:
                    x = (int)e.Event.RawX;
                    y = (int)e.Event.RawY;
                    break;

                case MotionEventActions.Move:
                    int nowX = (int)e.Event.RawX;
                    int nowY = (int)e.Event.RawY;
                    int movedX = nowX - x;
                    int movedY = nowY - y;
                    x = nowX;
                    y = nowY;
                    layoutParams.X += movedX;
                    layoutParams.Y += movedY;


                    windowManager.UpdateViewLayout(floatView, layoutParams);
                    break;

                default:
                    break;
            }
        }

        private void ButtonThrow_Click(object sender, EventArgs e)
        {
            ScriptManager.Throw(this, seekBar.Progress);

        }


        public override void OnDestroy()
        {
            base.OnDestroy();
            StopForeground(true);
            if (floatView != null)
            {
                windowManager.RemoveView(floatView);
            }
        }

    }

}