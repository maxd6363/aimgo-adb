﻿using AimGO.Overlay.Behind;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;

namespace AimGO.Overlay.Front
{
    [Service(Name = "com.companyname.aimgo.overlay.IAFloatingService")]
    class AIFloatingService : Service
    {
        public float MetersToPercentsRatio { get; set; }

        private WindowManagerLayoutParams layoutParams;
        private IWindowManager windowManager;
        private View floatView;
        private int x;
        private int y;
        private OCRManager OCRManager;
        private TextView textViewPokemon;
        private ImageView buttonScan;
        private ImageView buttonThrow;
        private ImageView buttonThrowRazzBerry;
        private ImageView buttonThrowPinapBerry;
        private ImageView buttonAppraise;
        private ImageView buttonOpenGift;
        private ImageView buttonSendGift;


        private Pokemon PokemonFound { get; set; }

        public override void OnCreate()
        {
            ShowFloatingWindow();
            base.OnCreate();
            OCRManager = new OCRManager(this);
            PokemonFound = null;
            MetersToPercentsRatio = 13.66f;
        }


        public override IBinder OnBind(Intent intent) => null;


        private void ShowFloatingWindow()
        {
            windowManager = GetSystemService(WindowService).JavaCast<IWindowManager>();
            LayoutInflater mLayoutInflater = LayoutInflater.From(ApplicationContext);

            floatView = mLayoutInflater.Inflate(Resource.Layout.AI_layout, null);

            floatView.Touch += FloatView_Touch;

            buttonScan = floatView.FindViewById<ImageView>(Resource.Id.buttonScan);
            buttonThrow = floatView.FindViewById<ImageView>(Resource.Id.buttonThrowPokeball);
            textViewPokemon = floatView.FindViewById<TextView>(Resource.Id.textViewPokemonFound);
            buttonThrowRazzBerry = floatView.FindViewById<ImageView>(Resource.Id.buttonThrowRazzBerry);
            buttonThrowPinapBerry = floatView.FindViewById<ImageView>(Resource.Id.buttonThrowPinapBerry);
            buttonAppraise = floatView.FindViewById<ImageView>(Resource.Id.buttonAppraise);
            buttonOpenGift = floatView.FindViewById<ImageView>(Resource.Id.buttonOpenGift);
            buttonSendGift = floatView.FindViewById<ImageView>(Resource.Id.buttonSendGift);


            buttonScan.Click += ButtonScan_Click;
            buttonThrow.Click += ButtonThrow_Click;
            buttonThrowRazzBerry.Click += ButtonThrowRazzBerry_Click;
            buttonThrowPinapBerry.Click += ButtonThrowPinapBerry_Click;
            buttonAppraise.Click += ButtonAppraise_Click;
            buttonOpenGift.Click += ButtonOpenGift_Click;
            buttonSendGift.Click += ButtonSendGift_Click;

            layoutParams = new WindowManagerLayoutParams
            {
                Type = WindowManagerTypes.ApplicationOverlay,
                Flags = WindowManagerFlags.NotFocusable,
                Width = Resources.DisplayMetrics.WidthPixels / 8,
                Height = (int)(Resources.DisplayMetrics.HeightPixels / 2.2),
                Format = Android.Graphics.Format.Translucent,
            };
            windowManager.AddView(floatView, layoutParams);

        }

        private void ButtonSendGift_Click(object sender, EventArgs e)
        {
            ScriptManager.SendGift(this);
        }

        private void ButtonOpenGift_Click(object sender, EventArgs e)
        {
            ScriptManager.OpenGift(this);
        }

        private void ButtonAppraise_Click(object sender, EventArgs e)
        {
            ScriptManager.Appraise(this);
        }

        private void ButtonThrowPinapBerry_Click(object sender, EventArgs e)
        {
            ScriptManager.Throw(this, Berry.Pinap);
        }

        private void ButtonThrowRazzBerry_Click(object sender, EventArgs e)
        {
            ScriptManager.Throw(this, Berry.Razz);
        }

        private void ButtonThrow_Click(object sender, EventArgs e)
        {
            int percent = 100;
            if (PokemonFound != null && PokemonFound.Distance != -1)
                percent = (int)(PokemonFound.Distance * MetersToPercentsRatio);
            ScriptManager.Throw(this, percent);
        }

        private void ButtonScan_Click(object sender, EventArgs e)
        {
            PokemonFound = OCRManager.Find(OCRManager.Recognize(this));
            Toast.MakeText(this, (PokemonFound != null ? "true" : "false"), ToastLength.Long).Show();
            textViewPokemon.Text = (PokemonFound == null ? Resources.GetString(Resource.String.AI_floating_pokemon_found) : PokemonFound.Name);
            buttonScan.Alpha = (PokemonFound == null ? 0.5f : 1.0f);
            buttonThrow.Alpha = (PokemonFound == null ? 0.5f : (PokemonFound.Distance == -1 ? 0.5f : 1.0f));
        }


        private void FloatView_Touch(object sender, View.TouchEventArgs e)
        {
            switch (e.Event.Action)
            {

                case MotionEventActions.Down:
                    x = (int)e.Event.RawX;
                    y = (int)e.Event.RawY;
                    break;

                case MotionEventActions.Move:
                    int nowX = (int)e.Event.RawX;
                    int nowY = (int)e.Event.RawY;
                    int movedX = nowX - x;
                    int movedY = nowY - y;
                    x = nowX;
                    y = nowY;
                    layoutParams.X += movedX;
                    layoutParams.Y += movedY;


                    windowManager.UpdateViewLayout(floatView, layoutParams);
                    break;

                default:
                    break;
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            StopForeground(true);
            if (floatView != null)
                windowManager.RemoveView(floatView);

        }
    }
}