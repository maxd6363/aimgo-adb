percent=$1

screen_size=$(wm size | cut -d : -f 2)
screen_width=$(echo $screen_size | cut -d x -f 1)
screen_height=$(echo $screen_size | cut -d x -f 2)
screen_width_middle=$(($screen_width/2))
screen_height_pokeball=$(echo "$screen_height*0.9 / 1" | bc)

pokeball_throw_destination=0
ms=200

map(){
	x=$1
	in_min=$2
	in_max=$3
	out_min=$4
	out_max=$5
	pokeball_throw_destination=$(echo "($x - $in_min) * ($out_max - $out_min) / ($in_max - $in_min) + $out_min" | bc)
}

map $percent 0 100 $screen_height_pokeball 0

echo "from : $screen_width_middle $screen_height_pokeball"
echo "to : $screen_width_middle $pokeball_throw_destination"

input swipe $screen_width_middle $screen_height_pokeball $screen_width_middle $pokeball_throw_destination $ms