screen_size=$(wm size | cut -d : -f 2)
screen_width=$(echo $screen_size | cut -d x -f 1)
screen_height=$(echo $screen_size | cut -d x -f 2)

click(){
	input touchscreen tap $1 $2
}

click $(($screen_width /2 )) $(($screen_height - 4 * $screen_height/10))
sleep 2
click $(($screen_width /2 )) $(($screen_height - $screen_height/7))
sleep 2
click $(($screen_width /2 )) $(($screen_height - $screen_height/20))
sleep 0.5
click $(($screen_width /2 )) $(($screen_height - $screen_height/20))
