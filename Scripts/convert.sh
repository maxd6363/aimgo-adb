rm /sdcard/gesture.patch 2> /dev/null

cat /sdcard/gesture.gest | while read -r line 
do
    for number in $line; do
        echo -n "$((0x$number)) " >> gesture.patch
    done
    echo >> gesture.patch
done