path="$1"
debug="$1"/debug.txt
rm $debug

rm $path/output.txt 
date +"%T.%3N" >> $debug
rm $path/screen.png 
date +"%T.%3N" >> $debug
sh $path/screenshot.sh $path/screen.png 
date +"%T.%3N" >> $debug
sh $path/recognize.sh $path/ 
date +"%T.%3N" >> $debug
cat $path/output.txt | tr -dc '[:alnum:]\n\r ' | tr -s "\n" | tr -s " " > $path/outputCleaned.txt
date +"%T.%3N" >> $debug