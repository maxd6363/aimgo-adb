berry="$1"


click(){
	input touchscreen tap $1 $2
}

selectBerry(){
	click $(($screen_width/10)) $(($screen_height - $screen_height/10))
}

throwBerry(){
	click $(($screen_width/2)) $(($screen_height - $screen_height/10))	
}

screen_size=$(wm size | cut -d : -f 2)
screen_width=$(echo $screen_size | cut -d x -f 1)
screen_height=$(echo $screen_size | cut -d x -f 2)

selectBerry
sleep 0.4

case $berry in
	1 )
		click $(($screen_width - $screen_width/10)) $(($screen_height - $screen_height/10))
		;;
	2 )
		click $(($screen_width/2)) $(($screen_height - $screen_height/10))
		;;
	3 )
		click $(($screen_width/10)) $(($screen_height - $screen_height/10))
		;;
esac

sleep 0.4
throwBerry