echo off

::adb push get.sh /sdcard
::adb push send.sh /sdcard
::adb push convert.sh /sdcard
::adb push throw.sh /sdcard

echo Please run adb as root

adb push throw.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/throw.sh"

adb push screenshot.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/screenshot.sh"

adb push recognize.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/recognize.sh"

adb push scheduler.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/scheduler.sh"

adb push berry.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/berry.sh"

adb push appraise.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/appraise.sh"

adb push open_gift.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/open_gift.sh"

adb push send_gift.sh /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/send_gift.sh"

adb push screen.uzn /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/screen.uzn"

adb push ..\Data\data_EN.csv /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/
adb shell -n "su -c dos2unix /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/data_EN.csv"

adb push ..\Data\eng.traineddata /storage/emulated/0/Android/data/com.thousanddust.aimgo.overlay/files/Download/

