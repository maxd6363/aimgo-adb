# AimGo - ADB

Utility for Pokemon GO [ROOT]


## Usage


[APK File](APK/com.thousanddust.aimgo.overlay-1.2.apk)

Click "Install", the button should turn green.

Click "Check Root" and allow root access with Magisk, the button should turn green.

Click "Check OCR", the button should turn green.

Click "Start" to launch the overlay

##  Demonstration

<p align="center">
    <img src="Documentation/Images/Demo.jpg" width="300px">
</p>


## Caution

- I'm not responsible if you get banned from the game.

## Devices
No guarantee it will work on every devices, only tested on the following devices :

```
[ROOT Magisk] Xiaomi Mi 9T -> Android 10 MSM Extended 
[ROOT Magisk] Xiaomi Pad 5 -> Android 11 Miui 12
[ROOT Magisk] Samsung Galaxy Note 4 -> Android 11 LineageOS

```

